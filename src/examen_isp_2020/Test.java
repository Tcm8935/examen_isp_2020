package examen_isp_2020;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.*;
import java.util.*;

public class Test extends JFrame {

    HashMap error = new HashMap();

    JLabel FileName;
    JLabel Text;
    JTextField tFileName;
    JTextArea tArea;
    JButton CopyFile;

    Test() {
        error.put("", "");
        error.put(" ", " ");

        setTitle("Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(750, 750);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        FileName = new JLabel("Enter file name: ");
        FileName.setBounds(10, 50, 250, height);

        Text = new JLabel("Enter text: ");
        Text.setBounds(10, 75, 250, height);

        tFileName = new JTextField();
        tFileName.setBounds(125, 50, width, height);

        CopyFile = new JButton("Copy text to specified file");
        CopyFile.setBounds(50, 625, 250, height);

        CopyFile.addActionListener(new Button());

        tArea = new JTextArea();
        tArea.setBounds(10, 100, 500, 500);

        add(FileName);
        add(tFileName);
        add(CopyFile);
        add(tArea);
        add(Text);

    }

    public static void main(String[] args) {
        new Test();
    }

    class Button implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String filename =  Test.this.tFileName.getText();

            if ( Test.this.error.containsKey(filename)) {
            	 Test.this.tArea.setText(null);
            	 Test.this.tArea.append("Please introduce a file name before pressing the button!\n");
            } else {
                    try {
                        File myObj = new File("C:\\Users\\tcm89\\Desktop\\testt\\" + filename + ".txt");
                        if (myObj.createNewFile()) {
                            String message = "File created: " + myObj.getName();
                            JOptionPane.showMessageDialog(new JFrame(), message, "SysInfo",
                                    JOptionPane.PLAIN_MESSAGE);
                        } else {
                            String message = "File already exists!";
                            JOptionPane.showMessageDialog(new JFrame(), message, "SysInfo",
                                    JOptionPane.PLAIN_MESSAGE);
                        }
                    } catch (IOException e1) {
                        String message = "An error occurred.";
                        JOptionPane.showMessageDialog(new JFrame(), message, "SysInfo",
                                JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                    try {
                        FileWriter myWriter = new FileWriter("C:\\Users\\tcm89\\Desktop\\testt\\" + filename + ".txt");
                        String txt =  Test.this.tArea.getText();
                        myWriter.write(txt);
                        myWriter.close();
                        String message = "Successfully wrote to the file.";
                        JOptionPane.showMessageDialog(new JFrame(), message, "SysInfo",
                                JOptionPane.PLAIN_MESSAGE);
                    } catch (IOException e2) {
                        String message = "An error occurred.";
                        JOptionPane.showMessageDialog(new JFrame(), message, "SysInfo",
                                JOptionPane.ERROR_MESSAGE);
                        e2.printStackTrace();
                    }
            }
        }

    }

}




